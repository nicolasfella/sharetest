#include "androidsharedialog.h"

#include <QDebug>


#ifdef Q_OS_ANDROID
#include <QAndroidJniObject>
#include <QtAndroid>
#endif

#define JSTRING(s) QAndroidJniObject::fromString(s).object<jstring>()
#define JSL(s) JSTRING(QStringLiteral(s))

void AndroidShareDialog::show()
{
#ifdef Q_OS_ANDROID
    QAndroidJniObject intent("android/content/Intent", "(Ljava/lang/String;)V", JSL("android.intent.action.SEND"));

    intent.callObjectMethod("setType", "(Ljava/lang/String;)Landroid/content/Intent;", JSL("text/plain"));
    intent.callObjectMethod("putExtra", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;", JSL("android.intent.extra.TEXT"), JSTRING(m_url));
    intent.callObjectMethod("putExtra", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;", JSL("android.intent.extra.EXTRA_SUBJECT"), JSTRING(m_title));

    QAndroidJniObject chooserIntent = QAndroidJniObject::callStaticObjectMethod("android/content/Intent", "createChooser", "(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;", intent.object(), JSTRING(m_dialogTitle));

    QtAndroid::startActivity(chooserIntent, 1);
#endif
}

void AndroidShareDialog::setTitle(const QString& title)
{
    if (m_title != title) {
        m_title = title;
        Q_EMIT titleChanged();
    }
}

QString AndroidShareDialog::title() const
{
    return m_title;
}

void AndroidShareDialog::setDialogTitle(const QString& dialogTitle)
{
    if (m_dialogTitle != dialogTitle) {
        m_dialogTitle = dialogTitle;
        Q_EMIT dialogTitleChanged();
    }
}

QString AndroidShareDialog::dialogTitle() const
{
    return m_dialogTitle;
}

void AndroidShareDialog::setUrl(const QString& url)
{
    if (m_url != url) {
        m_url = url;
        Q_EMIT urlChanged();
    }
}

QString AndroidShareDialog::url() const
{
    return m_url;
}

