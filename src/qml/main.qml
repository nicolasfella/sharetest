import QtQuick 2.0
import QtQuick.Controls 2.10

import org.kde.sharetest 1.0

ApplicationWindow
{
    visible: true
    width: 800
    height: 600

    Column {
        anchors.centerIn: parent

        TextField {
            id: urlField
            text: "https://kde.org"
        }

        Button {
            text: "Share"
            onClicked: {
                console.log("Aloha")
                shareDialog.show()
            }
        }
    }

    ShareDialog {
        id: shareDialog
        url: urlField.text
        title: "Shared from me"
        dialogTitle: "Share"
    }
}
