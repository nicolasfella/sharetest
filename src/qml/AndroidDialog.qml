import QtQuick 2.7
import org.kde.sharetest 1.0

Item {
    id: root

    function show() {
        shareDialog.show()
    }

    AndroidShareDialog {
        id: shareDialog

        dialogTitle: dialogTitle_
        title: title_
        url: url_
    }
}

