import QtQuick 2.0
import QtQuick.Controls 2.10

import org.kde.sharetest 1.0

Item {
    id: root

    property string url
    property string title
    property string dialogTitle

    function show() {
        loader.item.show()
    }

    Loader {
        id: loader

        property string url_: root.url
        property string title_: root.title
        property string dialogTitle_: root.dialogTitle

        source: Qt.platform.os === "android" ? "AndroidDialog.qml" : "PurposeDialog.qml"
    }
}
