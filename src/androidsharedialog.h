#pragma once

#include <QObject>

class AndroidShareDialog : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString dialogTitle READ dialogTitle WRITE setDialogTitle NOTIFY dialogTitleChanged)
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
public:

    QString title() const;
    void setTitle(const QString &title);

    QString dialogTitle() const;
    void setDialogTitle(const QString &title);

    QString url() const;
    void setUrl(const QString &url);

    Q_INVOKABLE void show();

Q_SIGNALS:
    void titleChanged();
    void dialogTitleChanged();
    void urlChanged();

private:
    QString m_title;
    QString m_dialogTitle;
    QString m_url;
};
