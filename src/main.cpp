#include <QGuiApplication>

#ifndef Q_OS_ANDROID
#include <QApplication>
#endif

#include <QQmlApplicationEngine>

#include "androidsharedialog.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
#ifdef Q_OS_ANDROID
    QGuiApplication app(argc, argv);
#else
    QApplication app(argc, argv);
#endif

    qmlRegisterType<AndroidShareDialog>("org.kde.sharetest", 1, 0, "AndroidShareDialog");

    QQmlApplicationEngine engine(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    return app.exec();
}
